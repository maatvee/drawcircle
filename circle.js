'use strict';
let radius = 200;
let decPlaces = 3;
let canvasWidth = 800;
let canvasHeight = 480;
let gridSize = 40;
let gridWidth = canvasWidth / gridSize;
let gridHeight = canvasHeight / gridSize;
let offsetH = canvasWidth / 2 - 1;
let offsetV = canvasHeight / 2 - 1;
let baseFont = "Helvetica Neue";
let baseFontSize = "12px";
let pointRadius = 17;
let pointArray = [];
let distanceArray = [];
let groupCount = 0;

let mouseX = 0;
let mouseY = 0;
let mouseDown = false;
let mouseHover = false;
let activePoint = -1;
//let count = 0;

const CHANNELS_PER_PIXEL = 4; //rgba

const NEG_INF = Number.NEGATIVE_INFINITY;
const POS_INF = Number.POSITIVE_INFINITY;

const canvas = document.getElementById('canvas');
const output = document.getElementById('output');

const ctx = canvas.getContext('2d');

canvas.width = canvasWidth;
canvas.height = canvasHeight;

// canvas.addEventListener('click', evt => {

    
// });

canvas.addEventListener('mousemove', evt => {
    
    mouseX = evt.offsetX;
    mouseY = evt.offsetY;
    //mouseHover = false;

    //console.log('hover: ', mouseHover);
    
    if (mouseHover && mouseDown) {
        movePoint(activePoint,mouseX,mouseY);
    }
    
    getHoverPoint();

    //runGroups();
    update();
    //runGroups();

    output.innerHTML = `x:${mouseX} y:${mouseY}`;
});

function movePoint(id,x,y){
    pointArray[id].x = x;
    pointArray[id].y = y;
}

canvas.addEventListener('mousedown', evt => {
    mouseDown = true;
    //console.log('mouseDown: ', mouseDown);

    
    // pointObject.x = evt.offsetX;
    // pointObject.y = evt.offsetY;
    // pointObject.index = pointArray.length;
    //drawCircle(evt.offsetX,evt.offsetY,50,canvas);

    if (!mouseHover && mouseDown) {

        let pointObject = {
            x: evt.offsetX,
            y: evt.offsetY,
            index: pointArray.length,
            selected: false
        };

        pointArray.push(pointObject);

        mouseHover = true;
        getHoverPoint();
        update();
    }
    //drawPoint(evt.offsetX,evt.offsetY,pointObject.index);
    //count++
    //output.innerHTML = `x:${evt.offsetX} y:${evt.offsetY}`;
    //console.log('pointArray: ', pointArray);

    
});

canvas.addEventListener('mouseup', evt => {
    mouseDown = false;
    //console.log('mouseDown: ', mouseDown);
    //runGroups();
});

function getDistance(p1,p2){
    return Math.sqrt(((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y)));
}


// function getAllDistances(){
//     for (let o = 0; o < pointArray.length; o++) {
//         for (let i = 0; i < o; i++) {


//         }
//     }
// }

function getHoverPoint(){
    mouseHover = false;
    for (let i = 0; i < pointArray.length; i++) {
        let point1 = {
            x: pointArray[i].x,
            y: pointArray[i].y,
        }

        let point2 = {
            x: mouseX,
            y: mouseY
        }

        let distance = getDistance(point1,point2);

        if (distance < pointRadius) {
            pointArray[i].selected = true;
            activePoint = i;
            mouseHover = true;
        } else {
            pointArray[i].selected = false;
            canvas.style.cursor = "default";
            mouseHover = false;
            activePoint = -1;
        }
    }
}

function drawPoint(x, y, id, pointColour = 'red', pointBodyColour = '') {
    threshold = document.getElementById('thresholdRange').value;
    basicCircle(ctx, x, y, threshold, '', 'rgba(0,0,255,0.3)', 1);
    basicCircle(ctx, x, y, pointRadius, pointBodyColour, pointColour, 1);
    setText(id, x, y + 5, '16px', pointColour);
    //runGroups();
}

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawGrid();
}

function update() {
    clearCanvas();

    for (let i = 0; i < pointArray.length; i++) {
        let _x = pointArray[i].x;
        let _y = pointArray[i].y;
        let _id = i;
        let bc,col;

        if (pointArray[i].selected) {
            bc = 'red';
            col = 'white';
            canvas.style.cursor = "pointer";
            mouseHover = true;
            activePoint = i;
        } else {
            bc = 'white';
            col = 'red';
            //canvas.style.cursor = "default";
            
        }
    
        drawPoint(_x, _y, _id,col,bc);        
        runGroups();
    }

    //getBoundingBox(pointArray);
    
}

function getBoundingBox(pointArray) {
    let minX = POS_INF;
    let minY = POS_INF;
    let maxX = NEG_INF;
    let maxY = NEG_INF;

    for (let i = 0; i < pointArray.length; i++) {

        let currMinX = pointArray[i].x - pointRadius;
        let currMaX = pointArray[i].x + pointRadius;
        let currMinY = pointArray[i].y - pointRadius;
        let currMaxY = pointArray[i].y + pointRadius;

        minX = minX < currMinX ? minX : currMinX;
        maxX = maxX > currMaX ? maxX : currMaX;
        minY = minY < currMinY ? minY : currMinY;
        maxY = maxY > currMaxY ? maxY : currMaxY;
    }

    let bounds = {
        left: minX,
        right: maxX - minX,
        top: minY,
        bottom: maxY - minY
    }

    //console.log('bounds: ', bounds);
    // draw the bounding box
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'rgba(0, 255, 0, 1)';
    ctx.strokeRect(bounds.left, bounds.top, bounds.right, bounds.bottom);
}

function setpixelated(context) {
    context['mozImageSmoothingEnabled'] = false;    /* Firefox */
    context['oImageSmoothingEnabled'] = false;      /* Opera */
    context['webkitImageSmoothingEnabled'] = false; /* Safari */
    context['msImageSmoothingEnabled'] = false;     /* IE */
    context['imageSmoothingEnabled'] = false;       /* standard */
}

function setText(string, x, y, baseFontSize = '12px', textColour = 'black') {
    ctx.font = baseFontSize + ' ' + baseFont;
    //ctx.strokeStyle = 'white';
    //ctx.lineWidth = 4;
    //ctx.strokeText(string, x, y);
    ctx.fillStyle = textColour;
    ctx.textAlign = 'center';
    ctx.fillText(string, x, y);
}

function setAxisLabels(gridSize, width, height) {

    for (let i = height + 1; i > height * -1; i -= gridSize * 2) {

        if (i <= radius) {
            setText((i / 200).toFixed(1).toString(), width + 10, (canvasHeight - i - height + 1));

            //console.log('i: ',i);
        }
    }
}

function circleFormula(x, r) {
    //let y = Math.sqrt( r*r - x*x ).toFixed(10);
    let y = Math.sqrt(r * r - x * x);
    //let y = 100;
    return y;
}

// function printOut(input,i){

//     $('#output').append('<p>x:' + i + '    y:' + input + '</p>');
// };

function drawArrowHeads(offH, offV, canvas, w, h) {

    (w == undefined) ? w = 10 : w;
    (h == undefined) ? h = 20 : h;

    let imageWidth = canvas.width;
    let imageHeight = canvas.height;
    let ctx = canvas.getContext('2d');
    //offH = offH;
    //let offV = offV;

    ctx.fillStyle = '#666';
    ctx.translate(0.5, 0.5);

    // ctx.imageSmoothingEnabled = false;

    setpixelated(ctx);


    ctx.beginPath();

    // X
    ctx.moveTo(h, offV + w);
    ctx.lineTo(0, offV);
    ctx.lineTo(h, offV - w);

    ctx.moveTo(imageWidth - h, offV - w);
    ctx.lineTo(imageWidth, offV);
    ctx.lineTo(imageWidth - h, offV + w);

    ctx.moveTo(offH - w, imageHeight - h);
    ctx.lineTo(offH, imageHeight);
    ctx.lineTo(offH + w, imageHeight - h);

    // Y
    ctx.moveTo(offH - w, h);
    ctx.lineTo(offH, 0);
    ctx.lineTo(offH + w, h);

    ctx.moveTo(offH - w, imageHeight - h);
    ctx.lineTo(offH, imageHeight);
    ctx.lineTo(offH + w, imageHeight - h);

    ctx.stroke();

    //console.log('arrowheads');


}

let outputArr = [];



// https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
// http://rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm#C.23

function basicCircle(ctx, x, y, radius, fill, stroke, strokeWidth) {
    ctx.beginPath()
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false)
    if (fill) {
        ctx.fillStyle = fill
        ctx.fill()
    }
    if (stroke) {
        ctx.lineWidth = strokeWidth
        ctx.strokeStyle = stroke
        ctx.stroke()
    }
}

function drawCircle(x0, y0, radius, canvas) {
    let x = radius;
    let y = 0;
    let decisionOver2 = 1 - x;   // Decision criterion divided by 2 evaluated at x=r, y=0
    let imageWidth = canvas.width;
    let imageHeight = canvas.height;
    let context = canvas.getContext('2d');
    let imageData = context.getImageData(0, 0, imageWidth, imageHeight);
    let pixelData = imageData.data;
    let makePixelIndexer = function (width) {
        return function (i, j) {
            let index = CHANNELS_PER_PIXEL * (j * width + i);
            //index points to the Red channel of pixel 
            //at column i and row j calculated from top left
            return index;
        };
    };
    let pixelIndexer = makePixelIndexer(imageWidth);
    let drawPixel = function (x, y) {
        let idx = pixelIndexer(x, y);
        pixelData[idx] = 255;	//red
        pixelData[idx + 1] = 0;	//green
        pixelData[idx + 2] = 0;//blue
        pixelData[idx + 3] = 255;//alpha
    };

    while (x >= y) {
        drawPixel(x + x0, y + y0);
        drawPixel(y + x0, x + y0);
        drawPixel(-x + x0, y + y0);
        drawPixel(-y + x0, x + y0);
        drawPixel(-x + x0, -y + y0);
        drawPixel(-y + x0, -x + y0);
        drawPixel(x + x0, -y + y0);
        drawPixel(y + x0, -x + y0);
        y++;
        if (decisionOver2 <= 0) {
            decisionOver2 += 2 * y + 1; // Change in decision criterion for y -> y+1
        } else {
            x--;
            decisionOver2 += 2 * (y - x) + 1; // Change for y -> y+1, x -> x-1
        }
    }

    context.putImageData(imageData, 0, 0);
}

function drawAxes() {
    ctx.fillStyle = '#666';
    ctx.fillRect(0, (canvasHeight / 2) - 1, canvasWidth, 1);
    ctx.fillRect((canvasWidth / 2) - 1, 0, 1, canvasHeight);
}


function drawGrid() {

    if (canvas.getContext) {

        for (let i = 0; i < gridHeight; i++) {

            for (let j = 0; j < gridWidth; j++) {

                //ctx.fillStyle = 'rgb(128,'+(128-j)+', 128)';
                //ctx.fillStyle = '#666';//'#FFF';
                ctx.fillStyle = '#FFF';
                ctx.fillRect(j * gridSize, i * gridSize, gridSize - 1, gridSize - 1);
            }
        }
    }
}

drawGrid();
//ctx.fillStyle = 'rgb(200, 0, 0)';
//ctx.fillRect(20,30,40,50);
//setpixelated(ctx);
//ctx.lineWidth = 1;
// ctx.strokeStyle = 'rgba(0, 0, 0, 0.5)'; //'black';
// ctx.strokeRect(40, 40, 40, 40);

//basicCircle(ctx, 100, 100, 50, '', 'red', 1);
//drawCircle(40,40,40,canvas);

//drawAxes();

//drawCircle(offsetH,offsetV,radius,canvas);

// drawArrowHeads(offsetH,offsetV,canvas);

// setAxisLabels(gridSize,offsetH,offsetV);

//let number = circleFormula(-10,10);



