// object-groups.js
'use strict';

let threshold;
let distLoopCount = 0;

// uPush()
// unique push, only add an element if it doesn't exist in the array.
// uPush(array, element)
// this version mutates the provided array

var uPush = (arr,elm) => {
    if (arr.indexOf(elm) == -1) {
        arr.push(elm);
    }
}

var getDist = distArr => {
    let result = [];
    let itemArr = [];
    let farItemArr = [];
    let farIndices = [];
    let nearInd = [];
    let farInd = [];
    let allInd = [];
    let count = 1;
    

    // for (let i = 0; i < distArr.length; i++ ) {
    //     allInd.push(i);
    // }

    for (let o = 0; o < distArr.length; o++ ) {
        for (let i = 0; i < o; i++ ) {
            
            // console.log(count,': ',o,i);
            // count++;

            let point1 = {
                x: distArr[o].x,
                y: distArr[o].y,
            }
            let point2 = {
                x: distArr[i].x,
                y: distArr[i].y,
            }

            let distance = getDistance(point1,point2);

            if (distance / 2 <= threshold) {
                result.push([o,i]);
                itemArr.push([distArr[o],distArr[i]]);
                uPush(nearInd,o);
                uPush(nearInd,i);
            } else {
                farItemArr.push([distArr[o],distArr[i]]);
                farIndices.push([o,i]);
            }
            
            distLoopCount++;

        }
    }

    // here we create the farInd (far indexes) by pushing the missing indexes into the farInd array
    for (let i = 0; i < distArr.length; i++ ) {

        if (nearInd.indexOf(i) == -1) {
            farInd.push(i);
            result.push([i]); // also include them in the result output for single point groups
        }  
        // uPush can't be used here since the test/condition array (nearInd) is different to the output
        // uPush(farInd,i);
        // uPush(result,i);
    }
    
    // console.log('distArr:',distArr);
    // console.log('allInd:',allInd);
    // console.log('nearInd:',nearInd.sort((a,b) => a - b));
    // console.log('farInd:',farInd);
    // console.log('itemArr:',itemArr);
    //console.log('farItemArr:',farItemArr);
    // console.log('result:',result);
    //console.log('farIndices:',farIndices);
    return result.filter( el => el.length > 0);
}

function fillArr(num){
    let arr = [];
    for (let i=0; i < num; i++) {
        arr[i] = [];
    }
    return arr
}

const testAB = (set,nextSet,tArr) => {
    let tempTest = false; // flag to indicate a match
    // copy the arrays to break the reference to the originals
    let setA = [...set];
    let setB = [...nextSet];

    // console.log('test tArr: ',tArr);

    for (let o = 0; o < setA.length; o++){
        for (let i = 0; i < setB.length; i++){
            //console.log('set[',o,']:',set[o],'  nextSet[',i,']:',nextSet[i]);
            if (setB[i] === setA[o]) {
                tempTest = true;

                // as soon as we find a match, we shove all the unique elements into tArr and break out of the loop;
                // this is also why we have to pass in tArr to the test function, so that it stays persistent
                for (let s2 = 0; s2 < setB.length; s2++){
                    // if (tArr.indexOf(setB[s2]) == -1){
                    //     tArr.push(setB[s2]);
                    // }     
                    
                    uPush(tArr,setB[s2]);
                }                       
                //break;
                continue;
            }
            //console.log('o:',setA[o],' i:',setB[i],' ',tempTest);
            //console.log('tArr: ',tArr);
        }
    };
    // returning an empty array instead of false doesn't work here
    return tempTest ? tArr : false; 
}

const inArrTest = (elm,arr) => {
    return arr.indexOf(elm) == -1;
} 

let finalArr = [];
let runCount = 0;
let runCountMax = 1000; // short circuit for recursive loop - just in case anything goes wrong with the termination check
let count = 0;

function getGroups(testData){

    // let finalArr = [];

    if (!testData.length) {
        //console.error('Empty Array Probably means that all the objects are too far away from each other and should all be in their own separate groups');
        return [];
    }

    if (runCount > runCountMax){ // stop recursive looping forever
        console.warn('max runCount exceeded, terminating:', runCount);
        return [];
    }

    const arrLen = testData.length; 
    let outGroup = []; //fillArr(arrLen);
    let noArr = []; //fillArr(arrLen);     
    let temp;     
    let tArr = [];             
        
    // here we put all of set into the tArr first
    // we need to pass in tArr into the testAB() function as it is called multiple times in the loop
    // it can't be initialised each time from inside the test function
    // if (testData && testData[0] && testData[0].length) {
    //     tArr = [...testData[0]]; // need to copy the array by using ...spread        
    // }

    tArr = testData && testData[0] && testData[0].length ? [...testData[0]] : []; 

    // console.log('---------------------------------------------------------------------'); 
    // console.log('runCount: ',runCount); 
    // console.log('testData input: ',testData);
    // console.log('---------------------------------------------------------------------');
    
    // loop over each other element starting at the index [1]     
    // the first element is at position [0] which all the others are compared to
    for (let i = 1; i < arrLen; i++){        
        
        // test could be an array or false
        let test = testAB(testData[0],testData[i],tArr);

        if (test){
                        
            // make sure outGroup[0] is a unique array each time if test is true    
            // need to assign the test using outGroup[0], not outGroup.push()
            // for some reason using push doesn't give you the correct output        
            outGroup[0] = [...testAB(testData[0],testData[i],tArr)];
            //outGroup.push([...testAB(testData[0],testData[i],tArr)]);
            temp = [...outGroup[0]];

            // console.log('temp:',temp);
            
        } else {                
            // if No Match            
            noArr[i] = [...testData[i]];            
        }            
    }
    // here we assign the temp value to the first position of the noArr 
    // noArr contains the items with no matches found with each of these items
    // if it doesn't exist (ie no matches at all were found), then an emtpy array is assigned instead
    noArr[0] = temp && temp.length ? [...temp] : [];

    // removes any empty arrays from outGroup    
    let cleanOutput = outGroup.filter(el => el.length > 0);
    // console.log('outGroup:', outGroup);
    // console.log('noArr:', noArr);
    
    let groupsRemain = noArr.filter(el => el.length > 0);
    
    // console.log('groupsRemain:', groupsRemain);
    // console.log('cleanOutput:', cleanOutput);

    if (cleanOutput.length < 1) {
        // console.log('nope no cleanOutput');
        // console.log('testData[0]: ', testData[0]);

        // if there is no output, the we add the first element of the current test to the finalArr
        // there are no more matching indices to add
        finalArr.push([...testData[0]]);
    }
        
    // if (groupsRemain.length < 1 && cleanOutput.length > 0){            
    //     finalArr.push([...cleanOutput[0]]);
    //     console.log('wez done: ',finalArr);
    //     console.log('---------------------------------------------------------------------');
    //     return [];        
    // } else 
   if (groupsRemain.length > 0) {           
        
        var cleanOutputFinal = [...groupsRemain];            
        //var result = [...getGroups(cleanOutputFinal)];
        finalArr = [...getGroups(cleanOutputFinal)];
        // console.log('go around input: ',cleanOutputFinal);

        // console.log('result: ',result);
        // console.log('count:',count,'go around input: ',cleanOutputFinal,' lets go around again: ', result);
        count++;
        
        //if (result.length > 0) {

            // this line is needed for this test(myTest): getGroups(myTest);
            // need to push the final value into finalArr for: getGroups(myTest);
            // but some reason is not needed for this test: var out = getGroups(testData);
            // TODO: need to investigate the termination conditions
            // var out2 = getGroups(myTest);

            //finalArr.push([...result[0]]);
        //}        
    }

    //return cleanOutput;

    runCount++; // count the number of times the getGroups function is called recursively, in case we need to terminate it.

    return finalArr;
}



//var distArr = [24,20,21,40,41,42,43,44,22,23];
//var myTest = getDist(distArr);
//const testData = [[4,5],[1,8],[5,6],[2,8],[4,3],[2,7]];

//var out = getGroups(testData);
//var out2 = getGroups(myTest);
//var out = getGroups([[1,8],[2,8],[2,7]]);


let thresholdRangeElm = document.getElementById('thresholdRange');
let rangeValElm = document.getElementById('rangeVal');

thresholdRangeElm.addEventListener('input',() => {

    threshold = document.getElementById('thresholdRange').value;

    rangeValElm.innerHTML = threshold;
    //runGroups();
    update();
    //runGroups();
});

thresholdRangeElm.addEventListener('change',() => {
    runGroups();
});

const groupsCounterElm = document.getElementById('groupsCounter');

function runGroups(){

    finalArr = [];
    runCount = 0;
    threshold = document.getElementById('thresholdRange').value;
    rangeValElm.innerHTML = threshold;
    var myTest = getDist(pointArray);
    //var out2 = getGroups(myTest);
    
    // console.log('myTest:', myTest);
    // console.log('finalArr: ', finalArr);

    //let outputArr = [...finalArr];
    let outputArr = [...getGroups(myTest)];
    //console.log('myTest.length:', myTest.length);
    //console.log('group count:', outputArr.length);

    groupCount = outputArr.length;

    groupsCounterElm.innerHTML = groupCount || 0;

    for (let i = 0; i < outputArr.length; i++) {
        let innerArr = outputArr[i];
        let currGroupArr = [];

        for (let o = 0; o < innerArr.length; o++) {
            currGroupArr.push(pointArray[innerArr[o]]);
        }

        getBoundingBox(currGroupArr);
    }

}

function logResults(){
    clear();
    distLoopCount = 0;
    finalArr = [];
    runCount = 0;
    var distanceArray = getDist(pointArray);
    var output = getGroups(distanceArray);
    
    console.log('pointArray: ', pointArray);
    console.log('distance Array:', distanceArray);
    console.log('distLoopCount: ', distLoopCount);
    console.log('output: ', output);
    console.log('runCount: ', runCount);
}


//console.log('out: ', out);
//console.log('out2: ', out2);
//var out2 = getGroups(myTest);
//var ver = testAB([1, 8, 2],[2, 8, 7]);
//console.log('ver: ', ver);
//console.log('out2: ', out2);
//console.log('out: ', out);
// var myTest = testAB([4,5,6,3],[1,8],[4,5,6,3]);
// console.log('myTest: ', myTest);

/*
References to similar problems and their solutions

https://stackoverflow.com/questions/480316/how-do-i-group-objects-in-a-set-by-proximity
https://en.wikipedia.org/wiki/K-means_clustering
https://en.wikipedia.org/wiki/Weber_problem
http://www.mqasem.net/vectorquantization/vq.html
https://stackoverflow.com/questions/23668456/clustering-objects-with-a-distance-matrix
*/